﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineaDeSuper.BE
{
    public interface IAcciones
    {
        int CrearCliente(Cliente cliente);
        bool CrearCaja(Caja nroCaja);
        bool CerrarCaja(Caja nroCaja);
        bool AgregarCliente(int idCliente, int nroCaja);
        bool AtenderCliente(int cliente, int nroCaja);
        List<Caja> ObtenerCajas();
        bool ActualizarClientXCaja(int nroCaja, int nroCajaBorrada);
        int ObtenerCajaMasVacia(int nroCaja);
        int ObtenerCliente(int nroCaja);
        Cliente ObtenerClienteAtendidos(int idCliente);
        int ObtenerClientesEnEspera(int nroCaja);
        bool ActualizarCajaClientes(int nroCaja);
        bool ActualizarCajaCerrada(int nroCajaMasVacia, int nroCaja, int nroEspera);
        bool BorrarClienteXCaja(int idCliente);
        bool AgregarClientesEspera(int nroCaja);
    }
}
