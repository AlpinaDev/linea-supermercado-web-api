﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineaDeSuper.BE
{
    public class SystemException : Exception
    {
        public SystemException()
        {
        }

        public SystemException(string message)
            : base(message)
        {
        }

        public SystemException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
