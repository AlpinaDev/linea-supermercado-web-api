﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineaDeSuper.BE
{
    public class Caja
    {
        public int NroCaja { get; set; }
        public string Descripcion { get; set; }
        //0 = caja cerrada, 1 = caja abierta
        public bool Estado { get; set; }
        public int Atendido { get; set; }
        public int Espera { get; set; }
    }
}
