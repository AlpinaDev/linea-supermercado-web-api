﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineaDeSuper.BE
{
    public class Cliente
    {
        public int IdCliente { get; set; }
        public string Nombre { get; set; }
    }
}
