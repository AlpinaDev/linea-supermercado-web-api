﻿using LineadeSuper.BLL;
using LineaDeSuper.BE;
using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Linq;

namespace LineaDeSuper
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnAbrirCaja_Click(object sender, EventArgs e)
        {
            try
            {
                dgvCajas.Enabled = true;
                btnCerrarCaja.Enabled = true;
                btnAgregarCliente.Enabled = true;
                string descripcion = Interaction.InputBox("Por favor ingrese una descripción para la caja: ");
                Caja nuevaCaja = new Caja();
                nuevaCaja.Descripcion = descripcion;
                nuevaCaja.Estado = true;
                CajaBLL.GetInstance().CrearCaja(nuevaCaja);
                CargarGrilla();
            }
            catch (Exception )
            {

                throw;
            }

        }

        private void CargarGrilla()
        {
            dgvCajas.DataSource = null;
            var result = CajaBLL.GetInstance().ObtenerCajas().ToList().Where(x => x.Estado).ToList();
            dgvCajas.DataSource = result;
            this.dgvCajas.Columns.Remove("Estado");
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            dgvCajas.AutoGenerateColumns = true;
            CargarGrilla();
        }

        private void btnCerrarCaja_Click(object sender, EventArgs e)
        {
            lbEspera.Text = "";
            if (dgvCajas.CurrentCell != null)
            {
            int cajaSeleccionada = (int)dgvCajas.Rows[dgvCajas.CurrentCell.RowIndex].Cells[0].Value;
            int enEspera = (int)dgvCajas.Rows[dgvCajas.CurrentCell.RowIndex].Cells[3].Value;
            Caja caja = new Caja();
            caja.NroCaja = cajaSeleccionada;
            caja.Estado = false;
            int cajaMasVacia = CajaBLL.GetInstance().ObtenerCajaMasVacia(cajaSeleccionada);
            if (cajaMasVacia !=0 || enEspera ==0)
            {
                CajaBLL.GetInstance().CerrarCaja(caja);
                CajaBLL.GetInstance().ActualizarClientXCaja(cajaMasVacia, caja.NroCaja);
                CajaBLL.GetInstance().ActualizarCajaCerrada(cajaMasVacia, caja.NroCaja, enEspera);
            }
            else
            {
                lbEspera.Text = "Debe atender a los clientes porque no hay otra caja disponible";
            }
            
            CargarGrilla();
            }
            
        }


        private void btnAgregarCliente_Click(object sender, EventArgs e)
        {
            if (dgvCajas.CurrentCell != null)
            {
                int cajaSeleccionada = (int)dgvCajas.Rows[dgvCajas.CurrentCell.RowIndex].Cells[0].Value;
                string nombreCliente = Interaction.InputBox("Por favor ingrese el nombre del cliente: ");
                Cliente cliente = new Cliente();
                cliente.Nombre = nombreCliente;
                int idCliente = CajaBLL.GetInstance().CrearCliente(cliente);
                int cajaMasVacia = CajaBLL.GetInstance().ObtenerCajaMasVacia(0);
                CajaBLL.GetInstance().AgregarCliente(idCliente, cajaMasVacia);
                CajaBLL.GetInstance().AgregarClientesEspera(cajaMasVacia);
                CargarGrilla();
            }
          
        }
        

        private void btnAtenderCliente_Click(object sender, EventArgs e)
        {
            lbAtendidos.Text = "";
            if (dgvCajas.CurrentCell != null)
            {
                int cajaSeleccionada = (int)dgvCajas.Rows[dgvCajas.CurrentCell.RowIndex].Cells[0].Value;
                int cliente = CajaBLL.GetInstance().ObtenerCliente(cajaSeleccionada);
                if (cliente != 0)
                {
                    CajaBLL.GetInstance().BorrarClienteXCaja(cliente);
                    CajaBLL.GetInstance().ActualizarCajaClientes(cajaSeleccionada);
                    string nombreCliente = CajaBLL.GetInstance().ObtenerClienteAtendidos(cliente).Nombre;
                    lbAtendidos.Text = "Sr " + nombreCliente + " sera atendido por la caja " + cajaSeleccionada;
                }
                else
                {
                    lbAtendidos.Text = "No hay mas clientes para atender";
                }
                CargarGrilla();
            }
            
        }

    }

}
