﻿using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Data;
using System.Collections.Generic;

namespace LineaDeSuper.DAL
{
    public class SqlUtils
    {
        public static SqlConnection Connection()
        {
            var connString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            var conn = new SqlConnection(connString);

            return conn;
        }

        public static bool Exec(string query, object param = null)
        {
            using (var connection = SqlUtils.Connection())
            {
                connection.Open();
                if (param == null)
                {
                    connection.Execute(query);
                }
                else
                {
                    connection.Execute(query, param);
                }

                return true;
            }
        }

        public static List<T> Exec<T>(string query, object param = null)
        {
            try
            {
                using (IDbConnection connection = SqlUtils.Connection())
                {
                    connection.Open();
                    var result = param == null ? (List<T>)connection.Query<T>(query) : (List<T>)connection.Query<T>(query, param);

                    return result;
                }
            }
            catch (System.Exception )
            {

                throw;
            }
            
        }
    }
}
