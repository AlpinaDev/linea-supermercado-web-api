﻿using LineaDeSuper.BE;
using LineaDeSuper.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LineadeSuper.BLL
{
    public class CajaDAL : IAcciones
    {
        private static CajaDAL _instancia;

        private CajaDAL()
        {
        }

        public static CajaDAL GetInstance()
        {
            if (_instancia == null)
            {
                _instancia = new CajaDAL();
            }

            return _instancia;
        }

        public int CrearCliente(Cliente cliente)
        {
            try
            {
                var query = " Insert into Clientes(Nombre) Values( @nombre);SELECT CAST(SCOPE_IDENTITY() as int)";
                return SqlUtils.Exec<int>(query, new
                {
                    @nombre = cliente.Nombre
                }).Single();
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al crear un cliente");
            }
        }

        public bool AgregarCliente(int idCliente, int nroCaja)
        {
            try
            {
                var query = "Insert into ClienteXCaja(IdCliente, Nrocaja) values(@idCliente, @nroCaja)";

                return SqlUtils.Exec(query, new
                {
                    @idCliente = idCliente,
                    @nroCaja = nroCaja
                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al agregar un cliente");
            }
        }

        public bool AtenderCliente(int idCliente, int nroCaja)
        {
            try
            {               
                var query = "Delete from ClienteXCaja where IdCliente = @idCliente and NroCaja = @nroCaja";

                return SqlUtils.Exec(query, new {
                    @idCliente = idCliente,
                    @nroCaja = nroCaja
                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al atender un cliente");
            }
        }

        public bool CerrarCaja(Caja caja)
        {
            try
            {
                var query = "Update Cajas set Estado = @estado Where NroCaja = @nrocaja";

                return SqlUtils.Exec(query, new
                {
                    @nroCaja = caja.NroCaja,
                    @estado = caja.Estado
                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al cerrar una caja");
            }
        }

        public bool ActualizarClientXCaja(int nroCaja, int nroCajaBorrada)
        {
            try
            {
                var query = "Update ClienteXCaja set NroCaja = @nroCaja Where NroCaja = @nroCajaBorrada";

                return SqlUtils.Exec(query, new
                {
                    @nroCaja = nroCaja,
                    @nroCajaBorrada = nroCajaBorrada
                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al actualizar una caja");
            }
        }


        public bool CrearCaja(Caja caja)
        {
            try
            {
                var query = "Insert into Cajas( Descripcion, Estado, Atendido, Espera) values( @descripcion, @estado, 0 ,0)";

                return SqlUtils.Exec(query, new {
                    @descripcion = caja.Descripcion,
                    @estado = caja.Estado
                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al crear una caja");
            }
        }
        

        public int ObtenerCajaMasVacia(int nroCaja)
        {
            try
            {
                var query = "SELECT  top 1 * FROM Cajas WHERE Estado = 1 and NroCaja != @NroCaja  order by Espera, NroCaja ";
                return SqlUtils.Exec<int>(query, new
                {
                    @nroCaja = nroCaja
                }).FirstOrDefault();
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al caja con menos clientes");
            }
        }
        public int ObtenerCliente(int nroCaja)
        {
            try
            {
                var query = "SELECT top 1 IdCliente FROM ClienteXCaja where NroCaja = @NroCaja order by NroCaja, IdCliente";

                return SqlUtils.Exec<int>(query, new
                {
                    @nroCaja = nroCaja
                }).FirstOrDefault();
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al Obtener cliente");
            }
        }

        public List<Caja> ObtenerCajas()
        {
            try
            {
                var query = "SELECT * from Cajas";

                var result = SqlUtils.Exec<Caja>(query);

                return result;
            }

            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al buscar caja");
            }
        }
        //arreglar
        public Cliente ObtenerClienteAtendidos(int idCliente)
        {
            try
            {
                var query = "SELECT Nombre FROM CLientes WHERE IdCliente = @IdCliente";

                return SqlUtils.Exec<Cliente>(query, new
                {
                    @IdCliente = idCliente
                }).FirstOrDefault();
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al Obtener clientes atendidos");
            }
        }

        public int ObtenerClientesEnEspera(int nroCaja)
        {
            try
            {
                var query = "SELECT COUNT(IdCliente) FROM [Supermercado].[dbo].[ClienteXCaja] WHERE NroCaja = @NroCaja";

                return SqlUtils.Exec<int>(query, new
                {
                    @nroCaja = nroCaja
                }).FirstOrDefault();

            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error Obtener clientes en espera");
            }
        }

        public bool ActualizarCajaClientes(int nroCaja)
        {
            try
            {
                var query = "UPDATE Cajas SET Atendido +=1, Espera -= 1  WHERE NroCaja = @nroCaja";

                return SqlUtils.Exec(query, new
                {
                    @nroCaja = nroCaja

                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al actualizar caja clientes");
            }
        }

        public bool ActualizarCajaCerrada(int nroCajaMasVacia, int nroCaja, int nroEspera)
        {
            try
            {
                var query = " UPDATE Cajas set Espera = 0  where NroCaja = @NroCaja; UPDATE Cajas set Espera += @nroEspera where NroCaja = @nroCajaMasVacia";


                return SqlUtils.Exec(query, new
                {
                    @nroCajaMasVacia = nroCajaMasVacia,
                    @nroCaja = nroCaja,
                    @nroEspera

                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al actualizar caja cerrada");
            }
        }
        public bool BorrarClienteXCaja(int idCliente)
        {
            try
            {
                var query = "DELETE FROM ClienteXCaja where IdCliente = @IdCliente";
                return SqlUtils.Exec(query, new
                {
                    @IdCliente = idCliente

                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al borrar cliente por caja");
            }
        }
        public bool AgregarClientesEspera(int nroCaja)
        {
            try
            {
                var query = "UPDATE Cajas SET Espera += 1  WHERE NroCaja = @nroCaja";

                return SqlUtils.Exec(query, new
                {
                    @nroCaja = nroCaja

                });
            }
            catch (LineaDeSuper.BE.SystemException)
            {
                throw new LineaDeSuper.BE.SystemException("Ocurrio un error al agregar cliente en espera");
            }
        }
    }
}
