﻿using LineaDeSuper.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineadeSuper.BLL
{
    public class CajaBLL : IAcciones
    {
        private CajaBLL()
        {               
        }

        private static CajaBLL _instance;

        public static CajaBLL GetInstance()
        {
            if (_instance == null)
            {
                _instance = new CajaBLL();
            }

            return _instance;
        }

        public bool AgregarCliente(int idCliente, int nroCaja)
        {
            return CajaDAL.GetInstance().AgregarCliente(idCliente, nroCaja);
        }

        public bool AtenderCliente(int idCliente, int nroCaja)
        {
            return CajaDAL.GetInstance().AtenderCliente(idCliente, nroCaja);
        }

        public bool CerrarCaja(Caja caja)
        {
            return CajaDAL.GetInstance().CerrarCaja(caja);
        }

        public bool ActualizarClientXCaja(int nroCaja, int nroCajaBorrada)
        {
            return CajaDAL.GetInstance().ActualizarClientXCaja(nroCaja, nroCajaBorrada);
        }

        public bool CrearCaja(Caja caja)
        {
            return CajaDAL.GetInstance().CrearCaja(caja);
        }

        public int CrearCliente(Cliente cliente)
        {
            return CajaDAL.GetInstance().CrearCliente(cliente);
        }

        public int ObtenerCajaMasVacia(int nroCaja)
        {
            return CajaDAL.GetInstance().ObtenerCajaMasVacia( nroCaja);
        }


        public int ObtenerCliente(int nroCaja)
        {
            return CajaDAL.GetInstance().ObtenerCliente(nroCaja);
        }

        public List<Caja> ObtenerCajas()
        {
            return CajaDAL.GetInstance().ObtenerCajas();
        }

        public Cliente ObtenerClienteAtendidos(int idCliente)
        {
            return CajaDAL.GetInstance().ObtenerClienteAtendidos(idCliente);
        }

        public int ObtenerClientesEnEspera(int nroCaja)
        {
            return CajaDAL.GetInstance().ObtenerClientesEnEspera(nroCaja);
        }
        public bool ActualizarCajaClientes(int nroCaja)
        {
            return CajaDAL.GetInstance().ActualizarCajaClientes(nroCaja);
        }

        public bool ActualizarCajaCerrada(int nroCajaMasVacia, int nroCaja, int nroEspera)
        {
            return CajaDAL.GetInstance().ActualizarCajaCerrada(nroCajaMasVacia, nroCaja, nroEspera);
        }

        public bool BorrarClienteXCaja(int idCliente)
        {
            return CajaDAL.GetInstance().BorrarClienteXCaja(idCliente);
        }

        public bool AgregarClientesEspera(int nroCaja)
        {
            return CajaDAL.GetInstance().AgregarClientesEspera(nroCaja);
        }
    }
}
